package net.therap.app;

import net.therap.service.*;
import net.therap.domain.*;

import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/5/20
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);

        String name = scan.nextLine();
        int age = scan.nextInt();

        Person person = new Person(name, age);

        List<ValidationError> errors = new ArrayList<ValidationError>();
        AnnotatedValidator validator = new AnnotatedValidator();

        validator.validate(person, errors);
        validator.print(errors);
    }
}
