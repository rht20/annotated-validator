package net.therap.service;

import net.therap.domain.*;

import java.util.*;
import java.lang.reflect.*;
import java.lang.annotation.*;
import java.util.regex.*;

/**
 * @author rakibul.hasan
 * @since 2/5/20
 */
public class AnnotatedValidator {

    public void validate (Person person, List<ValidationError> error) throws IllegalAccessException {
        String sampleStrObj = "";

        for (Field field : person.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            for (Annotation fieldAnnotation : field.getDeclaredAnnotations()) {
                if (fieldAnnotation instanceof Size) {
                    Size annotation = (Size) fieldAnnotation;

                    int minAgeLimit = annotation.min();
                    int maxAgeLimit = annotation.max();
                    String message = annotation.message();

                    if (field.getType().equals(sampleStrObj.getClass())) {
                        String str = (String) field.get(person);

                        if (!(str.length() >= minAgeLimit && str.length() <= maxAgeLimit)) {
                            error.add(new ValidationError("Length must be between " + minAgeLimit + " - " + maxAgeLimit));
                        }
                    } else {
                        int age = (Integer) field.get(person);

                        if (isLess(message)) {
                            if (age < minAgeLimit) {
                                message = message.replace("{min}", Integer.toString(minAgeLimit));
                                error.add(new ValidationError(message));
                            }
                        } else {
                            if (age > maxAgeLimit) {
                                message = message.replace("{max}", Integer.toString(maxAgeLimit));
                                error.add(new ValidationError(message));
                            }
                        }
                    }
                }
            }
        }
    }

    public void print(List<ValidationError> errors) {
        System.out.println("Output:");

        if (errors.isEmpty()) {
            System.out.println("Ok");
            return;
        }

        for (ValidationError verror : errors) {
            System.out.println(verror.getErrorMessage());
        }
    }

    public boolean isLess (String message) {
        Pattern pattern = Pattern.compile(".*less.*");
        Matcher matcher = pattern.matcher(message);

        return matcher.matches();
    }
}
